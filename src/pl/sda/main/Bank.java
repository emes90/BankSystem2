package pl.sda.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pl.sda.exceptions.AccountNotFoundException;
import pl.sda.exceptions.ClientNotFoundException;
import pl.sda.exceptions.NegativeAmountException;
import pl.sda.exceptions.NotEnoughMoneyException;
import pl.sda.model.AbstractAccount;
import pl.sda.model.AccountType;
import pl.sda.model.Client;
import pl.sda.model.CorporateAccount;
import pl.sda.model.CurrentAccount;
import pl.sda.model.SavingAccount;

public class Bank {

	private static final String CLIENT = "CLIENT";
	private static final String ACCOUNT = "ACCOUNT";
	private static final String COMMA = ",";
	private static final String FILE_NAME = "clients.txt";
	private static List<Client> clients = new ArrayList<>();

	public static Optional<Client> login(String login, String password) {
		return clients.stream()
			.filter(c -> c.getLogin().equals(login) && c.getPassword().equals(password))
			.findFirst();
	}
	
	public static List<Client> getClients() {
		return clients;
	}
	
	public static void payment(String accountNumber, double amount) {
		
		AbstractAccount a = searchAccountByAccountNumber(accountNumber);
		
		try {
			a.payment(amount);
		} catch (NegativeAmountException e) {
			e.printStackTrace();
			System.out.println("Wiadomosc z systemu: " + e.getMessage());
		}
	}
	
	private static AbstractAccount searchAccountByAccountNumber(String accountNumber) {
		
		return clients.stream()
			.flatMap(c -> c.getAccounts().stream())
			.filter(c -> c.getNumber().equals(accountNumber))
			.findFirst()
			.get();
	}
	
	public static Client getClientByAccountNumber(String accountNumber) throws ClientNotFoundException {
		for (Client client : clients) {
			for (AbstractAccount a : client.getAccounts()) {
				if (a.getNumber().equals(accountNumber)) {
					return client;
				}
			}
		}
		
		throw new ClientNotFoundException("Client with given account number: " + accountNumber + " not exists!");
	}
	
	public static void transfer(String src, String dst, double amount) throws AccountNotFoundException {
		
		AbstractAccount srcAccount = searchAccountByAccountNumber(src);
		
		AbstractAccount dstAccount = searchAccountByAccountNumber(dst);

		if (srcAccount == null || dstAccount == null) {
			throw new AccountNotFoundException("Nie odnaleziono konta src lub dst");
		}
		
		try {
			srcAccount.withdrawal(amount);
			dstAccount.payment(amount);
		} catch (NegativeAmountException | NotEnoughMoneyException e) {
			System.out.println("Wiadomosc z systemu: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Inny wyjatek: " + e.getMessage());
		}
	}
	
	public static void saveClients() {
		saveClients(clients);
	}
	
	public static void saveClients(List<Client> clients) {
		BufferedWriter bw = null;
		try {
			FileWriter fw = new FileWriter(FILE_NAME);
			bw = new BufferedWriter(fw);

			for (Client client : clients) {
				bw.write(CLIENT.concat(COMMA));
				bw.write(client.getName().concat(COMMA));
				bw.write(client.getSurname().concat(COMMA));
				bw.write(client.getAddress().concat(COMMA));
				bw.write(client.getLogin().concat(COMMA));
				bw.write(client.getPassword());
				bw.newLine();

				for (AbstractAccount account : client.getAccounts()) {
					bw.write(ACCOUNT.concat(COMMA));
					bw.write(account.getType().name().concat(COMMA));
					bw.write(account.getBalance() + COMMA);
					bw.write(account.getNumber());
					bw.newLine();
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bw.flush();
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void readClients() {
		BufferedReader br = null;

		try {
			FileReader fr = new FileReader(FILE_NAME);
			br = new BufferedReader(fr);

			String line = br.readLine();
			Client client = null;
			while (line != null) {

				String tab[] = line.split(COMMA);

				if (tab[0].equals(CLIENT)) {
					if (client != null) {
						clients.add(client);
					}

					client = new Client(tab[1], tab[2], tab[3], tab[4], tab[5]);

				} else if (tab[0].equals(ACCOUNT)) {
					AccountType type = AccountType.valueOf(tab[1]);
					AbstractAccount account = null;
					switch (type) {
					case CORPORATE:
						account = new CorporateAccount(tab[3], Double.parseDouble(tab[2]));
						break;
					case CURRENT:
						account = new CurrentAccount(tab[3], Double.parseDouble(tab[2]));
						break;
					case SAVING:
						account = new SavingAccount(tab[3], Double.parseDouble(tab[2]));
						break;
					default:
						break;
					}

					client.getAccounts().add(account);
				}

				line = br.readLine();
			}
			clients.add(client);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
