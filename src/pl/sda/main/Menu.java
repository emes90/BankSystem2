package pl.sda.main;

import java.util.Optional;
import java.util.Scanner;

import pl.sda.model.AbstractAccount;
import pl.sda.model.Client;

public class Menu {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println("Podaj login");
		String username = scanner.next();
		System.out.println("Podaj haslo");
		String password = scanner.next();

		Bank.readClients();

		Optional<Client> op = Bank.login(username, password);

		Client client = null;

		if (op.isPresent()) {
			client = op.get();
		} else {
			System.out.println("Bledny login lub haslo!");
			System.exit(0);
		}

		int input = 0;
		do {
			System.out.println("1. Wplata");
			System.out.println("2. Przelew");
			System.out.println("3. Wyplata");
			System.out.println("4. Wypisz");
			System.out.println("0. Zakoncz");

			input = scanner.nextInt();

			switch (input) {
			case 1:
				payment(client);
				break;
			case 2: {
			}
				break;
			case 3: {
			}
				break;
			case 4:
				printClient(client);
				break;
			}

		} while (input != 0);

		Bank.saveClients();
	}

	private static AbstractAccount printAccounts(Client client) {
		System.out.println("Wybierz konto do wykonania operacji:");

		for (int i = 0; i < client.getAccounts().size(); i++) {
			System.out.println(i + 1 + ". " + client.getAccounts().get(i).getType() + ", balance: "
					+ client.getAccounts().get(i).getBalance());
		}

		int input = scanner.nextInt();

		return client.getAccounts().get(input - 1);
	}

	private static void payment(Client client) {
		AbstractAccount account = printAccounts(client);

		System.out.print("Podaj kwote: ");
		double amount = scanner.nextDouble();

		Bank.payment(account.getNumber(), amount);
	}

	private static void printClient(Client client) {
		System.out.println("Imie i Nazwisko: " + client.getName() + " " + client.getSurname());
		System.out.println("Adres: " + client.getAddress());

		for (int i = 0; i < client.getAccounts().size(); i++) {
			System.out.println(i + 1 + ". " + client.getAccounts().get(i).getType() + ", balance: "
					+ client.getAccounts().get(i).getBalance() + ", number: "
					+ client.getAccounts().get(i).getNumber());
		}
	}
}
