package pl.sda.model;

import java.util.ArrayList;
import java.util.List;

public class Client extends User {

	private String address;
	private List<AbstractAccount> accounts;
	
	public Client(String name, String surname, String password, String address) {
		super(name, surname, password);
		this.address = address;
		accounts = new ArrayList<>();
		accounts.add(new CurrentAccount());
	}
	
	public Client(String name, String surname, String address, String login, String password) {
		this(name, surname, password, address);
		this.login = login;
		this.accounts.clear();
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<AbstractAccount> getAccounts() {
		return accounts;
	}

	@Override
	public String toString() {
		return "Client [address=" + address + ", accounts=" + accounts + ", name=" + name + ", surname=" + surname
				+ ", login=" + login + ", password=" + password + "]";
	}

}
