package pl.sda.model;

public enum AccountType {

	CURRENT, SAVING, CORPORATE
}
