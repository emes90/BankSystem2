package pl.sda.model;

import pl.sda.exceptions.NegativeAmountException;
import pl.sda.exceptions.NotEnoughMoneyException;

public class CorporateAccount extends AbstractAccount {

	public CorporateAccount() {
		super();
		this.type = AccountType.CORPORATE;
	}
	
	public CorporateAccount(String accountNumber, double balance) {
		this();
		this.number = accountNumber;
		this.balance = balance;
	}
	
	@Override
	public void withdrawal(double amount) throws NegativeAmountException, NotEnoughMoneyException {
		
		if (balance >= amount + 1) {
			super.withdrawal(amount);
		} else {
			throw new NotEnoughMoneyException("Brak kasy na oplaty");
		}
	}
}
