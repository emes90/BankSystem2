package pl.sda.model;

import pl.sda.exceptions.NegativeAmountException;

public class SavingAccount extends AbstractAccount {

	public SavingAccount() {
		super();
		this.type = AccountType.SAVING;
	}
	
	public SavingAccount(String accountNumber, double balance) {
		this();
		this.number = accountNumber;
		this.balance = balance;
	}
	
	@Override
	public void payment(double amount) throws NegativeAmountException {
		super.payment(amount);
		
		balance = balance * 1.1; //kapitaliacja po wp�acie
	}
}
