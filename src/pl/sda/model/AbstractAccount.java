package pl.sda.model;

import pl.sda.exceptions.NegativeAmountException;
import pl.sda.exceptions.NotEnoughMoneyException;

public abstract class AbstractAccount {

	protected static int numberGenerator = 1;
	protected String number;
	protected double balance;
	protected AccountType type;
	
	public AbstractAccount() {
		this.number = "" + numberGenerator;
		numberGenerator++;
		this.balance = 0.0;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getNumber() {
		return number;
	}

	public AccountType getType() {
		return type;
	}
	
	public void payment(double amount) throws NegativeAmountException {
		//obs�uga sytuacji, w kt�rej amount jest < 0
		
		if (amount < 0) {
			throw new NegativeAmountException("Podana kwota jest mniejsza od zera!");
		}
		
		balance = balance + amount;
	}
	
	public void withdrawal(double amount) throws NegativeAmountException, NotEnoughMoneyException {
		//obs�uga sytuacji, w kt�rej amount jest < 0
		//obs�uga sytuacji, w kt�rej amount > balance
		
		if (amount < 0) {
			throw new NegativeAmountException("Podana kwota jest mniejsza od zera!");
		} else if (amount > balance) {
			throw new NotEnoughMoneyException("Za malo srodkow na koncie!");
		}
		
		balance = balance - amount;
	}

	@Override
	public String toString() {
		return "AbstractAccount [number=" + number + ", balance=" + balance + ", type=" + type + "]";
	}
}
