package pl.sda.model;

public class CurrentAccount extends AbstractAccount {

	public CurrentAccount() {
		super();
		this.type = AccountType.CURRENT;
	}

	public CurrentAccount(String accountNumber, double balance) {
		this();
		this.number = accountNumber;
		this.balance = balance;
	}
}
